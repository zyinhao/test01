### 项目名称：test01
### 项目描述：使用maven搭建web项目
### 功能点：maven基本知识、maven创建项目、springMVC的引入

## 一：环境
    JDK：1.8
	开发工具：idea
	maven：3.5

## 二：maven基本概念
>* 代理：proxy：
>* 本地资源库：localRepository：保存下载的jar包
>* 中央存储库：central repository：保存所有的jar包
>* 镜像：mirror：默认中央仓库：http://repo1.maven.org/maven2/；镜像仓库-阿里云：http://maven.aliyun.com/nexus/content/groups/public/
>* 组件三要素：groupId（组织名称，一般为公司域名）、artifactId（组件名称）、version（版本）；比如：com.zyinhao:test:1.0.0
>* 组件范围：scope：指定组件在哪个生命周期命令别使用；比如：compile（默认）、provided、runtime、test（执行生命周期test时依赖）、system、import；
>* 依赖：dependency：通过依赖三要素，自动到“中央存储库”下载对应的jar包以及其依赖到“本地资源库”，并引入至项目；
>* 插件：plugin：类型 构建（build） 报告（reporting）：eg 编译代码、创建jar\war包、单元测试、创建项目文档等：
>>* org.apache.maven.plugins:maven-compiler-plugin：指定JDK版本编译项目
>* 命令：clean（清除target）、package（打包）、install（安装至本地资源库，方便其他项目依赖）；完整命令：clean（清除）、validate（校验）、compile（编译）、test（测试）、package（打包）、verify（验证）、install（安装）、site（站点：项目文档等）、deploy（发布）；详情查看“maven 生命周期”：https://www.yiibai.com/maven/maven_build_life_cycle.html#article-start
>* 侧面：profile：

## 三：maven创建java web项目
### 1:创建maven web项目
![创建maven web项目](https://gitee.com/zyinhao/test01/raw/master/images/1create_maven_project.jpg)

### 2:pom.xml引入spring mvc
![pom.xml引入spring mvc](https://gitee.com/zyinhao/test01/raw/master/images/2add_springmvc_dependency.jpg)

### 3:配置URL映射，以及返回内容
![配置URL映射，以及返回内容](https://gitee.com/zyinhao/test01/raw/master/images/3add_url_content.jpg)

### 4:idea集成tomcat，并运行项目
![idea集成tomcat](https://gitee.com/zyinhao/test01/raw/master/images/4idea_add_tomcat.jpg)

### 5:运行项目
![运行项目](https://gitee.com/zyinhao/test01/raw/master/images/5run_project.jpg)

## 四：maven高级知识

## 五：参考
* [IntelliJ idea创建Spring MVC的Maven项目](https://www.cnblogs.com/winner-0715/p/5294917.html): maven项目创建、引入spring mvc、增加相关文件、idea集成tomcat
* [maven教程](https://www.yiibai.com/maven/):易百教程，比较详细
* [官网](http://maven.apache.org/)：软件下载、教程
* [官网pom.xml解析](http://maven.apache.org/pom.html)：pom.xml组成要素解析
